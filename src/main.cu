#include <iostream>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>

#include <cuda.h>
#include <curand_kernel.h>
#include "cuPrintf.cu"
#define CUDART_INF_F __int_as_float(0x7f800000)

#include "hr_time.h"
#include "my_cutil.h"
#include "MersenneTwister.h"

using namespace std;
#define PI_F 3.141592654f 

typedef vector< long double > ldArray1D;
typedef vector< vector< long double > > ldArray2D;
typedef vector< vector< vector <long double > > > ldArray3D;


double sigMoid(double v){

	return 1/(1+exp(-v));
}

long double F1(ldArray2D& R, int Nd, int p) { // Sphere
	long double Z=0,  Xi;

	for(int i=0; i<Nd; i++){
		Xi = R[p][i];
		Z += Xi*Xi;
	}
	return -Z;

}

long double F2(ldArray2D& R, int Nd, int p) {  
	long double Z=0,  Xi;

	for(int i=0; i<Nd; i++){
		Xi = R[p][i];
		Z += (pow(Xi,2) - 10 * cos(2*PI_F*Xi) + 10);
	}
	return -Z;
}
long double F3(ldArray2D& R, int Nd, int p) {
	long double Z, Sum, Prod, Xi;

    Z = 0; Sum = 0; Prod = 1;
    
	for(int i=0; i<Nd; i++){
		Xi = R[p][i];
		Sum  += Xi*Xi;
		Prod *= cos(Xi/sqrt((double)i)+1)/4000.0f; 
		
		if(isnan(Prod)) Prod = 1;
    }
	
	Z = Sum - Prod;
	
	return -Z;
}
long double F4(ldArray2D& R, int Nd, int p) {
	long double Z=0, Xi, XiPlus1;

	for(int i=0; i<Nd-1; i++){
		Xi = R[p][i];
		XiPlus1 = R[p][i+1];
		Z = Z + (100*(XiPlus1-Xi*Xi)*(XiPlus1-Xi*Xi) + (Xi-1)*(Xi-1));
	}
	return -Z;
}

__global__ void setup_kernel(curandState *state){
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    curand_init(clock(), id, 0, &state[id]);
}

__global__ void initR(float* R, int Nd, int xMin, int xMax, curandState *state){
    int p  = blockIdx.y;
    int i  = threadIdx.x + (blockIdx.x*blockDim.x);
    float r;
    curandState localState = state[i];
     
	r = xMin + curand_uniform(&localState)*(xMax-xMin);   
	if(curand_uniform(&localState) < 0.5){
		r = -r;
	}
	R[p*Nd+i] = r;
	state[i] = localState;
}

__global__ void initV(float* V, int Nd, int vMin, int vMax, curandState *state){
    int p  = blockIdx.y;
    int i  = threadIdx.x + (blockIdx.x*blockDim.x);
    float Vel;
    curandState localState = state[i];
        
	Vel = vMin + curand_uniform(&localState)*(vMax-vMin);   
	if(curand_uniform(&localState) < 0.5){
		Vel = -Vel;
	}
	V[p*Nd+i] = Vel;
	state[i] = localState;
}

__global__ void initPBest(float* pBestPosition, int Nd){
    int p  = blockIdx.y;
    int i  = threadIdx.x + (blockIdx.x*blockDim.x);
        
	pBestPosition[p*Nd+i] = 0;
}

__global__ void init_Np(float* M, float* pBestValue, float* gBestValue){
    int p  = blockIdx.x;  // Current Block

    M[p]            = -CUDART_INF_F;
    pBestValue[p]   = -CUDART_INF_F;
    *gBestValue     = -CUDART_INF_F;
}

__global__ void init_Nd(float* gBestPosition){
    int i  = blockIdx.x; // Current Thread
    gBestPosition[i] = 0;
}

__global__ void evalFitness(float* R, float* M, int Nd){
    float Mp;
    int   p  = blockIdx.x;  // Current Block
    
    Mp = 0;
    for(int i=0; i<Nd; i++){
        Mp += (R[p*Nd+i]*R[p*Nd+i]);
    }   
    M[p] = -Mp;
}

__global__ void updateLocalBest(float* R, float* M, float* pBestValue, float* pBestPosition, int Nd){
    int p  = blockIdx.y;
    int i  = threadIdx.x + blockIdx.x * blockDim.x;
    float Mp = M[p];
    float pBest = pBestValue[p];
    
    __shared__ bool changed;
    changed = false;
    
    if(threadIdx.x == 0){ // First thread in block
        if(Mp > pBestValue[p]){
            pBestValue[p] = Mp;
            changed = true;
        }
    }
    __syncthreads();
    if(changed){
        pBestPosition[p*Nd+i] = R[p*Nd+i];
    }
}

__global__ void updateGlobalBest(float* R, float* M, float* gBestValue, float* gBestPosition, int Np, int Nd){
    int id = threadIdx.x + blockIdx.x * blockDim.x;

    if(id == 0){
        for(int p=0; p<Np; p++){
            if(M[p] > *gBestValue){
                *gBestValue = M[p];
                for(int i=0; i<Nd; i++){
                    gBestPosition[i] = R[p*Nd+i];
                }
            }
        }
    }   
}
__global__ void updateR(float* R, float* V, curandState *state, int Nd, float xMin, float xMax){
    int p  = blockIdx.y;  // Current Block
    int i  = threadIdx.x + blockIdx.x * blockDim.x;
    curandState localState = state[i];
    
    float pos = R[p*Nd+i];
    pos = pos + V[p*Nd+i];

	if(pos > xMax) pos = xMin + curand_uniform(&localState)*(xMax-xMin);
    if(pos < xMin) pos = xMin + curand_uniform(&localState)*(xMax-xMin);
    
    R[p*Nd+i] = pos; 
    state[i] = localState;   
}

__global__ void updateV(float* V, float* R, float* pBestPosition, float* gBestPosition, curandState *state, float chi, int Nd, int vMin, int vMax, int C1, int C2){
    int p  = blockIdx.y;  // Current Particle
    int i  = threadIdx.x + blockIdx.x * blockDim.x;
    float Vel = V[p*Nd+i];
    float Pos = R[p*Nd+i];
    curandState localState = state[i];
    
    // Original PSO
    Vel = chi * Vel + C1*curand_uniform(&localState)*(pBestPosition[p*Nd+i] - Pos) + C2*curand_uniform(&localState)*(gBestPosition[i] - Pos);              

    if(Vel > vMax) Vel = vMin + curand_uniform(&localState)*(vMax-vMin);
    if(Vel < vMin) Vel = vMin + curand_uniform(&localState)*(vMax-vMin);
    
    V[p*Nd+i] = Vel;
    state[i] = localState;
}

void PSO(int Np, int Nd, int Nt, long double xMin, long double xMax, long double vMin, long double vMax,long double (*objFunc)(ldArray2D& ,int, int), int& numEvals, string functionName){
    cudaPrintfInit();
    
    float gBestValue;
    float C1, C2;
	float w, wMax, wMin;
    int lastStep, blocksPerParticle, threadsPerBlock;
    MTRand mt;
    
	dim3 _gridDim;
	
	CStopWatch timer, timer1;
	long double positionTime = 0, fitnessTime = 0, velocityTime = 0, totalTime = 0, initTime = 0;
		
	float *R_GPU, *V_GPU, *M_GPU,
	      *pBestPosition_GPU, *gBestPosition_GPU, 
	      *pBestValue_GPU, *gBestValue_GPU;
	curandState *devStates;
	
	gBestValue = -INFINITY;
	C1 = 2.05; C2 = 2.05;
	wMin = 0.4; wMax = 0.9;
	lastStep = Nt;
    numEvals = 0;
    positionTime = 0; fitnessTime = 0; velocityTime = 0; totalTime = 0;
    
	// Allocate on GPU
    CudaSafeCall(cudaMalloc((void**) &V_GPU,             Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &R_GPU,             Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &M_GPU,             Np   *sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &pBestPosition_GPU, Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &pBestValue_GPU,    Np*   sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &gBestPosition_GPU, Nd*   sizeof(float)));
    CudaSafeCall(cudaMalloc(         &gBestValue_GPU,          sizeof(float)));
	
    if(Nd < 448){
    	blocksPerParticle  = ceil((float)Nd/Np);
    	threadsPerBlock = Nd/blocksPerParticle; 
    }else{
    	blocksPerParticle  = ceil((float)Nd/448.0);
	    threadsPerBlock = Nd/blocksPerParticle; 
    }

    _gridDim.x = blocksPerParticle;
    _gridDim.y = Np;
    
    timer1.startTimer();
    
    CudaSafeCall(cudaMalloc((void **)&devStates, Np*Nd*sizeof(curandState)));    
    setup_kernel<<< _gridDim, threadsPerBlock>>> (devStates);
    initR       <<< _gridDim, threadsPerBlock>>> (R_GPU, Nd, xMin, xMax, devStates);    CudaCheckError();
    initV       <<< _gridDim, threadsPerBlock>>> (V_GPU, Nd, vMin, vMax, devStates);    CudaCheckError();
    initPBest   <<< _gridDim, threadsPerBlock>>> (pBestPosition_GPU, Nd);               CudaCheckError();
    init_Np     <<< Np, 1>>>(M_GPU, pBestValue_GPU, gBestValue_GPU);  CudaCheckError();
    init_Nd     <<< Nd, 1>>>(gBestPosition_GPU);                                        CudaCheckError();
    
    timer1.stopTimer();
    initTime += timer1.getElapsedTime();
    
    evalFitness <<< Np, 1 >>> (R_GPU, M_GPU, Nd); CudaCheckError();    
    numEvals += Np; 
    for(int j=1; j<Nt; j++){
    
		timer.startTimer();
        updateR<<<_gridDim, threadsPerBlock>>>(R_GPU, V_GPU, devStates, Nd, xMin, xMax); CudaCheckError();
		timer.stopTimer();
		positionTime += timer.getElapsedTime();

		// Evaluate Fitness
		timer.startTimer();
		evalFitness      <<< Np, 1 >>> (R_GPU, M_GPU, Nd);  CudaCheckError();  
        updateGlobalBest <<< 1,  1 >>> (R_GPU, M_GPU, gBestValue_GPU, gBestPosition_GPU, Np, Nd); CudaCheckError();
        updateLocalBest  <<< _gridDim, threadsPerBlock >>> (R_GPU, M_GPU, pBestValue_GPU, pBestPosition_GPU, Nd);     CudaCheckError();
        CudaSafeCall(cudaMemcpy(&gBestValue, gBestValue_GPU, sizeof(float), cudaMemcpyDeviceToHost));     
        numEvals += Np;
        timer.stopTimer();
		fitnessTime += timer.getElapsedTime();
        
        if(gBestValue >= -0.0001){
            lastStep = j;
    	    break;
	    }

        // Update Velocities
		timer.startTimer();
		w = wMax - ((wMax-wMin)/Nt) * j;
        updateV <<< _gridDim, threadsPerBlock >>> (V_GPU, R_GPU, pBestPosition_GPU, gBestPosition_GPU, devStates, w, Nd, vMin, vMax, C1, C2); CudaCheckError();
        timer.stopTimer();
		velocityTime += timer.getElapsedTime();
	} // End Time Steps
    timer1.stopTimer();
    totalTime += timer1.getElapsedTime();

    cudaPrintfEnd();
    CudaSafeCall(cudaFree(pBestPosition_GPU));
    CudaSafeCall(cudaFree(pBestValue_GPU));
    CudaSafeCall(cudaFree(gBestPosition_GPU));
    CudaSafeCall(cudaFree(gBestValue_GPU));
    CudaSafeCall(cudaFree(R_GPU));
    CudaSafeCall(cudaFree(V_GPU));
    CudaSafeCall(cudaFree(M_GPU));
    CudaSafeCall(cudaFree(devStates));
        
	cout    << functionName << " "
            << gBestValue   << " " 
            << Np           << " "
            << Nd           << " "
            << lastStep     << " "
            << numEvals     << " "
            << positionTime << " "
            << fitnessTime  << " "
            << velocityTime << " "
            << totalTime    << endl;
}

void run_PSO(long double xMin, long double xMax, long double vMin, long double vMax, long double (*rPtr)(ldArray2D& , int, int), string functionName){

    int Np, Nd, Nt, numEvals;
    int NdMin, NdMax, NdStep;
    int NpMin, NpMax, NpStep;
    cudaDeviceProp deviceProp;
    int deviceCount, dev;
    
    NdMin = 32; NdMax = 4096; NdStep = 2;
    NpMin = 32; NpMax = 4096; NpStep = 2;
    cudaSetDevice(3);
    
    Nt = 1000; 
    for(Np=NpMin; Np <= NpMax; Np *= NpStep){
        for(Nd=NdMin; Nd <= NdMax; Nd *= NdStep){
            for(int x=0; x<1; x++){
                cudaGetDeviceProperties(&deviceProp, dev);
                cudaGetDeviceCount(&deviceCount);            
                PSO(Np, Nd, Nt, xMin, xMax, vMin, vMax, rPtr, numEvals, functionName);
            }
        }
    }
//    int Nt, deviceCount, dev, numEvals;
//    cudaDeviceProp deviceProp;
//    vector<int> Np (3, 0);vector<int> Nd (3, 0);
//    
//    Np[0] = 100; Np[1]=500; Np[2]=1000;
//    Nd[0] = 100; Nd[1]=500; Nd[2]=1000;
//    Nt = 10000;
//    
//    cudaSetDevice(1);  
//    for(int i=0; i< Np.size(); i++){
//        for(int j=0; j< Nd.size(); j++){
//            for(int x=0; x<10; x++){
//                cudaGetDeviceCount(&deviceCount);
//                cudaGetDeviceProperties(&deviceProp, dev);
//                PSO(Np[i], Nd[j], Nt, xMin, xMax, vMin, vMax, rPtr, numEvals, functionName);
//            }
//        }
//    }
}
int main(){

    int deviceCount=0, dev=0;
	long double (*rPtr)(ldArray2D& , int, int) = NULL;
	long double xMin, xMax, vMin, vMax;
    cudaDeviceProp deviceProp;
    
    cudaGetDeviceCount(&deviceCount);
    cudaGetDeviceProperties(&deviceProp, dev);
    
    cout << "Function, Fitness, Np, Nd, Last Step, Evals, Position Time, Fitness Time, Velocity Time, Total Time" << endl;
    
    rPtr = &F1;
    xMin = -100; xMax = 100;
    vMin = -100; vMax = 100;
    run_PSO(xMin, xMax, vMin, vMax, rPtr, "F1");
    
//    rPtr = &F2; Np = 120;
//    xMin = -10; xMax = 10;
//    vMin = -10; vMax = 10;
//    run_PSO(Np, xMin, xMax, vMin, vMax, rPtr, "F2");
//    
//    rPtr = &F3; Np = 180;
//    xMin = -600; xMax = 600;
//    vMin = -600; vMax = 600;
//    run_PSO(Np, xMin, xMax, vMin, vMax, rPtr, "F3");

//    rPtr = &F4; Np = 180;
//    xMin = -10; xMax = 10;
//    vMin = -10; vMax = 10;
//    run_PSO(Np, xMin, xMax, vMin, vMax, rPtr, "F4");

    rPtr = NULL;

	return 0;
}
